export class TextureArrayManager {

    static textureArrayObject = {};

    static loadAssets (app) {

        app.loader.baseUrl = "assets";

        app.loader.add("playerGreen", "animations/player-green.png")
        .add("playerBlue", "animations/player-blue.png")
        .add("playerGray", "animations/player-gray.png")
        .add("rocket", "animations/rocket.png")
        .add("explosion", "animations/explosion.png");

        app.loader.load();
    }

    static generateTextureArrayObject (app) {

        const playerTextureArray = this
        .#createTextureArray(new PIXI.BaseTexture.from(app.loader.resources.playerGreen.url), 8, 52, 63, 1);

        const stillRocketTexture = this
        .#createTextureArray(new PIXI.BaseTexture.from(app.loader.resources.rocket.url), 1, 25, 33, 1);
        
        const rocketTextureArray = this
        .#createTextureArray(new PIXI.BaseTexture.from(app.loader.resources.rocket.url), 4, 25, 49, 1);
        
        rocketTextureArray.push(stillRocketTexture[0]);
        
        const explosionTextureArray = this
        .#createTextureArray(new PIXI.BaseTexture.from(app.loader.resources.explosion.url), 40, 128, 128, 0);
        
        
        this.textureArrayObject.rocketTextureArray = rocketTextureArray;
        
        this.textureArrayObject.explosionTextureArray = explosionTextureArray;
        
        this.textureArrayObject.playerTextureArray = playerTextureArray;
    }

    static #createTextureArray(spriteSheet, frames, w, h, margin) {

        const resutlArray = [];

        for (let i = 0; i < frames; i++) {

            resutlArray[i] = new PIXI.Texture(spriteSheet, 
                new PIXI.Rectangle(((i * margin) + (i * w)), 0, w, h));
        }
        
        return resutlArray;
    }
}