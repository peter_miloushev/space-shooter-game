import { TextureArrayManager } from './texture-array-manager.js';
import { Player } from './player/player.js';
import { EnemySpawnManager } from './enemy/enemy-spawn-manager.js';
import { Explosion } from './weapons/explosion/explosion.js';
import { StageManager } from './stage-manager.js';
import { TickerManager } from './ticker-manager.js';
import { HUD } from './head-up-display.js';

export class GameManager {

    static player;

    static ticker = new PIXI.Ticker();

    
    static getReady (app) {
        
        this.#loadAssets(app);
        
        this.generateTextureArrayObject(app);
        
        this.ticker.add((delta) => this.gameLoop(delta, app));

        TickerManager.tickers.gameManager.push(this.ticker)
        
        this.ticker.start();

        this.createPlayer(app);
    }

    static #loadAssets (app) {

        TextureArrayManager.loadAssets(app);
    }

    static generateTextureArrayObject (app) {

        TextureArrayManager.generateTextureArrayObject(app);
    }

    static createPlayer (app) {

        const player = new Player(450, 450, TextureArrayManager.textureArrayObject.playerTextureArray, 100, app);
        
        this.player = player;

        StageManager.addToStage(player.still, app);
        HUD.showInfo(app);
    }

    static gameLoop(delta, app) {

        EnemySpawnManager.spawnAnEnemy(app, this.player, 0);
        HUD.updateInfo(this.player.score, 
                    this.player.health, 
                    this.player.rocketLauncher.active? 
                    this.player.rocketLauncher.ammo:
                    this.player.laserGun.ammo);
    }

    static events(string, e, app) {

        if(string === 'pointermove') {

            this.player.events(string, e, app);
        }

        switch(string) {

            case 'mousedown':
                this.player.events(string, e, app);

                break;
            case 'mouseup':
                this.player.events(string, e, app);

                break;
            case 'enemyhit':
                EnemySpawnManager.destroyAnEnemy(e, app);
                this.player.score += 1;

                const explosion = new Explosion(e.detail.x, e.detail.y, TextureArrayManager.textureArrayObject.explosionTextureArray, 1);
                explosion.explode(app);

                break;
            case 'playerhit':

                this.player.health > 20? this.player.health -= 20: this.player.health = "game over!";
                break;
            case 'wheel':

                this.player.events(string, e, app);

                break;
            case 'beam':

                this.player.events(string, e, app);
                break;
        }
    }
}