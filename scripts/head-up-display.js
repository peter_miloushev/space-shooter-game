export class HUD {

    static score = new PIXI.Text("score");
    static health = new PIXI.Text("health")
    static magazine = new PIXI.Text("magazine");

    static showInfo (app) {

        this.score.x = 825;
        this.score.y = 75;
        this.score.anchor.set(0.5);
        this.score.alpha = 0.5;
        this.score.style = new PIXI.TextStyle({

            fill: 0x239915
        });
        app.stage.addChild(this.score);

        this.health.x = 75;
        this.health.y = 120;
        this.health.anchor.set(0.5);
        this.health.alpha = 0.5;
        this.health.style = new PIXI.TextStyle({

            fill: 0x239915
        });
        app.stage.addChild(this.health);

        this.magazine.x = 75;
        this.magazine.y = 75;
        this.magazine.anchor.set(0.5);
        this.magazine.alpha = 0.5;
        this.magazine.style = new PIXI.TextStyle({

            fill: 0x239915,
        });
        app.stage.addChild(this.magazine);
    }

    static updateInfo (score, health, magazine) {

        if (typeof(health) === "number") {

            this.score.text = "score: " + score;
            this.health.text = "health: " + health;
            this.magazine.text = "ammo: " + magazine;
        } else {

            this.health.x = 450;
            this.health.text = health;
            this.score.text = "";
            this.magazine.text = "";
        }
    }
}