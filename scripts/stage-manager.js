export class StageManager {

    static onStage = [];

    static addToStage (object, app) {

        app.stage.addChild(object);
        object.play();
        this.onStage.push(object);
    }

    static removeFromStage (object, app) {

        if (object) {

            app.stage.removeChild(object);
            object.stop();
    
            const resultStage = this.onStage.filter(o => o!=object);
            this.onStage = resultStage.slice(0);
        }
    }
}