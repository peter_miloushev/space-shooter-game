import { GameManager } from './game-manager.js';
import { TickerManager } from './ticker-manager.js';

const gameDiv = document.getElementById("game-view");
const gameCanvas = document.getElementById("game-canvas");

GameManager.canvas = gameCanvas;


const generateApp = function () {
    
    const app = new PIXI.Application({
        view: gameCanvas,
        width: 900,
        height: 900
    });

    gameDiv.appendChild(app.view);

    app.renderer.backgroundColor = 0x551135;

    app.stage.interactive = true;
    
    return app;
}

let app = generateApp();

GameManager.getReady(app);

gameCanvas.addEventListener("contextmenu", e => e.preventDefault());
app.stage.on("pointermove", e => GameManager.events("pointermove", e, app));
gameCanvas.addEventListener("wheel", e => GameManager.events("wheel", e, app));
gameCanvas.addEventListener("mousedown", e => GameManager.events("mousedown", e, app));
gameCanvas.addEventListener("mouseup", e => GameManager.events("mouseup", e, app));
gameCanvas.addEventListener("enemyhit", e => GameManager.events("enemyhit", e, app));
gameCanvas.addEventListener("playerhit", e => GameManager.events("playerhit", e, app));
gameCanvas.addEventListener("beam", e => GameManager.events("beam", e, app));

const playAgain = function () {

    //console.log(TickerManager.tickers)

    //TickerManager.tickers.forEach(t => t.stop());
    //TickerManager.tickers.forEach(t => t.destroy());

    //window.location.reload();
    //app.stop();
    //app.loader.destroy();
    //app.renderer.destroy();
    //app.ticker.destroy();
    //app.stage.destroy();

    //app.destroy();
    //app = generateApp();
    //GameManager.getReady(app);
}