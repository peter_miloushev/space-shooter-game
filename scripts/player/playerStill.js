export class PlayerStill extends PIXI.AnimatedSprite {

    constructor(x, y, rotation, textures) {

        super(textures);

        this.x = x;
        this.y = y;
        this.rotation = rotation;
        this.anchor.set(0.5, 0.65);
        this.loop = false;
        this.active = true;
    }
}