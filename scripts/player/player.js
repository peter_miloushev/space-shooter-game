import { PlayerWalking } from './playerWalking.js';
import { PlayerStill } from './playerStill.js';
import { RocketLauncher } from '../weapons/rocket-launcher.js';
import { laserGun  } from '../weapons/laser-gun.js';
import { StageManager } from '../stage-manager.js';
import { TickerManager } from '../ticker-manager.js';

export class Player {

    constructor(x, y, textures, health, app) {

        this.team = 1;
        this.health = health;
        this.x = x;
        this.y = y;
        this.ticker = new PIXI.Ticker();
        TickerManager.tickers.player.push(this.ticker);
        this.ticker.add(() => this.playerMovement(app));
        this.ticker.start();
        this.rotation = 0;
        this.playerWalking = false;
        this.score = 0;
        this.aimX = 0;
        this.aimY = 0;
        
        this.rocketLauncher = new RocketLauncher(5, this.team);

        this.laserGun = new laserGun(1000, this.team);

        this.walking = new PlayerWalking(this.x, this.y, this.rotation, textures, 0.25);
        
        this.still = new PlayerStill(this.x, this.y, this.rotation, [textures[0]]);
    }

    playerMovement(app) {

        if (this.playerWalking) {
    
            if (this.walking.active === false) {
    
                StageManager.removeFromStage(this.still, app);
                StageManager.addToStage(this.walking, app);
                this.walking.active = true;
                this.still.active = false;
            }
    
            this.#walk(1);
        }
    
        if (!this.playerWalking) {
    
            if (this.still.active === false) {
    
                StageManager.removeFromStage(this.walking, app);
                StageManager.addToStage(this.still, app);
                this.still.active = true;
                this.walking.active = false;
            }
        }
    }

    #walk(speed) {

        this.x += Math.sin(this.rotation) * speed;
        this.y -= Math.cos(this.rotation) * speed;
        this.walking.x = this.x;
        this.walking.y = this.y;
        this.still.x = this.x;
        this.still.y = this.y; 
    }

    lookAtAim(e) {

        const pos = e.data.global;
        this.aimX = pos.x;
        this.aimY = pos.y;
        const yDelta = this.y - pos.y;
        const xDelta = pos.x - this.x;
        this.rotation = Math.atan2(xDelta, yDelta);
        this.walking.rotation = this.rotation;
        this.still.rotation = this.rotation;
    }

    fire (app, target) {

        this.rocketLauncher.active? this.rocketLauncher.fire(app, this, target): this.laserGun.fire(app, this, target);
    }

    events(string, e, app) {

        if(string === 'pointermove') {

            this.lookAtAim(e);
        }

        switch(string) {

            case 'mousedown':
                e.button === 2? 
                this.playerWalking = true: e.button === 0? 
                this.fire(app, {x: e.offsetX, y: e.offsetY}): console.log(StageManager.onStage);

                break;
            case 'mouseup':
                e.button === 2? 
                this.playerWalking = false: this.laserGun.beam = false;

                break;
            case 'enemyhit':
                EnemySpawnManager.destroyAnEnemy(e, app)

                break;
            case 'wheel':
                this.rocketLauncher.active = !this.rocketLauncher.active;
                this.laserGun.active = !this.laserGun.active;

                const activeRocket = this.rocketLauncher.magazine.filter(r => r.active);
                const activeBeam = this.laserGun.magazine.filter(b => b.active);

                if (this.rocketLauncher.active) {

                    if (activeBeam.length) {

                        StageManager.removeFromStage(activeBeam[0].still, app);
                    }

                    if (activeRocket.length) {

                        StageManager.addToStage(activeRocket[0].still, app);
                    }
                }

                if (this.laserGun.active) {

                    if (activeBeam.length) {

                        StageManager.addToStage(activeBeam[0].still, app);
                    }

                    if (activeRocket.length) {

                        StageManager.removeFromStage(activeRocket[0].still, app);
                    }
                }

                break;
            case 'beam':
                
                if (this.laserGun.beam) {

                    setTimeout(() => this.fire(app, {x: this.aimX, y: this.aimY}), 10);
                }

                break;
        }
    }
}