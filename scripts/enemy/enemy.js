import { EnemyWalking } from './enemy-walking.js';
import { EnemyStill } from './enemy-still.js';
import { RocketLauncher } from '../weapons/rocket-launcher.js';
import { laserGun  } from '../weapons/laser-gun.js';
import { TickerManager } from '../ticker-manager.js';

export class Enemy {

    constructor(x, y, rotation, textures) {

        this.team = 0;
        this.ticker = new PIXI.Ticker();
        TickerManager.tickers.enemy.push(this.ticker);
        this.x = x;
        this.y = y;
        this.rotation = rotation;
        this.hasTarget = false;
        this.delay = false;
        this.dead = false;
        
        this.rocketLauncher = new RocketLauncher(5, this.team);

        this.laserGun = new laserGun(100, this.team);

        this.walking = new EnemyWalking(this.x, this.y, this.rotation, textures, 0.25);
        
        this.still = new EnemyStill(this.x, this.y, this.rotation, [textures[0]]);
    }

    walk(speed) {

        this.x += Math.sin(this.rotation) * speed;
        this.y -= Math.cos(this.rotation) * speed;
        this.walking.x = this.x;
        this.walking.y = this.y;
        this.still.x = this.x;
        this.still.y = this.y; 
    }

    fight(target, app) {

        if (this.hasTarget && !this.delay) {

            this.delay = true;
            this.#destroyTarget(target, app)

            setTimeout(() => {

                this.delay = false;
            }, 1000)
        } else {

            this.#findTarget(target);
        }
    }

    #findTarget(target) {

        const yDelta = this.y - target.y;
        const xDelta = target.x - this.x;
        let targetRotation = Math.atan2(xDelta, yDelta);

        if (this.rotation - targetRotation < 0.03 && this.rotation - targetRotation > -0.03) {

            this.hasTarget = true;
        } else {

            this.rotation += 0.01;
            this.rotation > Math.PI? this.rotation -= 2 * Math.PI: 
            this.walking.rotation = this.rotation;
            this.still.rotation = this.rotation;
        }
    }

    #destroyTarget(target, app) {

        this.rocketLauncher.fire(app, this, target);
    }
}