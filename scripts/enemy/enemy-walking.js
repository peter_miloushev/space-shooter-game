export class EnemyWalking extends PIXI.AnimatedSprite {

    constructor(x, y, rotation, textures, speed) {

        super(textures);

        this.x = x;
        this.y = y;
        this.tint = 0xAA3355;
        this.rotation = rotation;
        this.anchor.set(0.5, 0.65);
        this.animationSpeed = speed;
        this.active = false;
    }
}