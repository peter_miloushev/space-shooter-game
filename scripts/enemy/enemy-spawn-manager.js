import { TextureArrayManager } from "../texture-array-manager.js";
import { Enemy } from "./enemy.js";
import { StageManager } from "../stage-manager.js";
import { GameManager } from "../game-manager.js";

export class EnemySpawnManager {

    static enemies = [];
    static spawning = false;

    static spawnAnEnemy(app, player, numberOfEnemies) {

        
        if (!this.spawning && this.enemies.length < numberOfEnemies) {
            
            this.spawning = true;
            setTimeout(() => {

                const rotation = Math.random() * 2 * Math.PI;
                const [x, y] = this.#generateEnemySpawnLocation(player);
                const enemy = new Enemy(x, y, rotation, TextureArrayManager.textureArrayObject.playerTextureArray);
                this.enemies.push(enemy);
                StageManager.addToStage(enemy.still, app);
                enemy.ticker.add(() => enemy.fight(player, app));
                enemy.ticker.start();
                this.spawning = false;
            }, Math.random() * 4000 + 4000);
        }
    }

    static destroyAnEnemy(event, app) {

        const enemy = event.detail.enemy;
        StageManager.removeFromStage(enemy.still, app);

        const enemyActiveRocket = enemy.rocketLauncher.magazine.filter(r => r != null && r.active === true)[0];
        
        enemy.ticker.destroy();
        enemy.dead = true;
        
        if (enemyActiveRocket) {

            StageManager.removeFromStage(enemyActiveRocket.still, app);
        }

        const resultEnemies = this.enemies.filter(e => e!=enemy);
        this.enemies = resultEnemies.slice(0);
    }

    static #generateEnemySpawnLocation(player) {

        const borderOffset = 75;

        let x = Math.random() * 900;
        let y = Math.random() * 900;

        if ((x > borderOffset) 
        && (x < GameManager.canvas.width - borderOffset) 
        && (y > borderOffset) 
        && (y < GameManager.canvas.height - borderOffset)) {

            const distance = Math.sqrt(Math.pow((x - player.x), 2) + 
            Math.pow((y - player.y), 2));
            
            if (distance < 225) {
                
                [x, y] = this.#generateEnemySpawnLocation(player);
            }
        } else {

            [x, y] = this.#generateEnemySpawnLocation(player);
        }
    
        return [x, y];
    }
}