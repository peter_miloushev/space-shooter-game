export class Explosion {

    constructor(x, y, textures, speed) {

        this.animation = new PIXI.AnimatedSprite(textures);

        this.animation.x = x;
        this.animation.y = y;
        this.animation.anchor.set(0.5);
        //this.animation.scale.set(0.35);
        this.animation.animationSpeed = speed;
        this.animation.loop = false;
    }

    explode(app) {

        app.stage.addChild(this.animation);
        this.animation.play();
    }
}