import { TextureArrayManager } from "../texture-array-manager.js";
import { Rocket } from "./rocket/rocket.js";
import { StageManager } from "../stage-manager.js";

export class RocketLauncher {

    constructor(magazineSize, team) {

        this.team = team;
        this.active = true;
        this.magazineSize = magazineSize;
        this.ammo = 0;
        this.magazine = [];
        this.reload = false;
        this.shotDelay = false;
    }

    fire (app, carrier, target) {

        if (this.ammo) {
    
            const rocket = this.magazine[0];
    
            if (rocket && !this.shotDelay) {
    
                StageManager.removeFromStage(rocket.still, app);
                StageManager.addToStage(rocket.launched, app);
                rocket.aimAtTarget(target);
                rocket.launch = true;
                this.shotDelay = true;
                const ammoLeft = this.magazine.filter(r => r != rocket);
                this.magazine = ammoLeft;

                setTimeout(() => {

                    this.shotDelay = false;
                    this.#keepItLoaded(app, carrier);
                    this.ammo -= 1;
                }, 500)
            }
        } else {
            
            if (!this.reload) {
                
                this.reload = true;

                setTimeout(() => {

                    this.reload = false;
                    this.#keepItLoaded(app, carrier);
                }, 1250)
            }
        }
    }

    #keepItLoaded(app, carrier) {

        if (this.active && !this.reload && !this.ammo) {


            for (let i = 0; i < this.magazineSize; i++) {

                const rocket = new Rocket(TextureArrayManager.textureArrayObject.rocketTextureArray, 0.25, this.team);

                this.magazine.push(rocket);

                this.ammo++;
            }                      
        }

        if (this.magazine.length) {

            this.#loadARocket(app, this.magazine[0], carrier);
        }
    }

    #loadARocket(app, rocket, carrier) {

        if (!carrier.dead) {

            rocket.ticker.add(() => rocket.position(carrier, app));
            rocket.ticker.start();
            StageManager.addToStage(rocket.still, app);
            rocket.active = true;        
        }
    }
}