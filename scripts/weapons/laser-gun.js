import { TextureArrayManager } from "../texture-array-manager.js";
import { Beam } from "./beam/beam.js";
import { StageManager } from "../stage-manager.js";
import { GameManager } from "../game-manager.js";

export class laserGun {

    constructor(magazineSize, team) {

        this.team = team;
        this.active = false;
        this.magazineSize = magazineSize;
        this.ammo = 0;
        this.magazine = [];
        this.reload = false;
        this.shotDelay = false;
        this.beam = false;
    }

    fire (app, carrier, target) {

        this.beam = true;

        if (this.ammo) {
    
            const rocket = this.magazine[0];
    
            if (rocket && !this.shotDelay) {
    
                StageManager.removeFromStage(rocket.still, app);
                StageManager.addToStage(rocket.launched, app);
                rocket.aimAtTarget(target);
                rocket.launch = true;
                this.shotDelay = true;
                const ammoLeft = this.magazine.filter(r => r != rocket);
                this.magazine = ammoLeft;

                setTimeout(() => {

                    this.shotDelay = false;
                    this.#keepItLoaded(app, carrier);
                    this.ammo -= 1;

                    const event = new CustomEvent("beam");
    
                    GameManager.canvas.dispatchEvent(event);
                }, 10)
            }
        } else {
            
            if (!this.reload) {
                
                this.reload = true;

                setTimeout(() => {

                    this.reload = false;
                    this.#keepItLoaded(app, carrier);
                }, 1250)
            }
        }
    }

    #keepItLoaded(app, carrier) {

        if (this.active && !this.reload && !this.ammo) {


            for (let i = 0; i < this.magazineSize; i++) {

                const rocket = new Beam(TextureArrayManager.textureArrayObject.explosionTextureArray, 0.25, this.team);

                this.magazine.push(rocket);

                this.ammo++;
            }                      
        }

        if (this.magazine.length) {

            this.#loadARocket(app, this.magazine[0], carrier);
        }
    }

    #loadARocket(app, rocket, carrier) {

        if (!carrier.dead) {

            rocket.ticker.add(() => rocket.position(carrier, app));
            rocket.ticker.start();
            StageManager.addToStage(rocket.still, app);
            rocket.active = true;
        }
    }
}