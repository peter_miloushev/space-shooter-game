export class Beamready extends PIXI.AnimatedSprite {

    constructor(x, y, rotation, textures, speed, team) {

        super(textures);

        this.team = team;
        this.x = x;
        this.y = y;
        this.rotation = rotation;
        this.anchor.set(0.5);
        this.scale.set(0.35);
        this.animationSpeed = speed;
    }
}