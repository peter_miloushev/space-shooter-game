import { EnemySpawnManager } from '../../enemy/enemy-spawn-manager.js';
import { BeamAction } from './beam-action.js';
import { Beamready } from './beam-ready.js';
import { GameManager } from '../../game-manager.js';
import { StageManager } from '../../stage-manager.js';
import { TickerManager } from '../../ticker-manager.js';

export class Beam {

    constructor(textures, speed, team) {

        this.team = team;
        this.launch = false;
        this.x = 0;
        this.y = 0;
        this.rotation = 0;
        this.active = false;
        this.ticker = new PIXI.Ticker();
        TickerManager.tickers.rocket.push(this.ticker);

        this.launched = new BeamAction(this.x, this.y, this.rotation, [textures[0]], speed, this.team);

        this.still = new Beamready(this.x, this.y, this.rotation, textures.slice(0, 1), speed, this.team);
    }

    position(carrier, app) {

        if (!this.launch) {

            const offsetForward = 45;
            const offsetRIght = - 12;
                
            const offsetX = Math.sin(carrier.rotation + (Math.PI / 2)) * offsetRIght;
            const offsetY = - Math.cos(carrier.rotation + (Math.PI / 2)) * offsetRIght;
                
            const rocketSpawnX = carrier.x + Math.sin(carrier.rotation) * offsetForward;
            const rocketSpawnY = carrier.y - Math.cos(carrier.rotation) * offsetForward;
                
            this.x = rocketSpawnX + offsetX;
            this.y = rocketSpawnY + offsetY;
            this.rotation = carrier.rotation;
        
            this.still.x = this.x;
            this.still.y = this.y;
            this.still.rotation = this.rotation;
            this.launched.rotation = this.rotation; 
                
            this.launched.x = this.x;
            this.launched.y = this.y;

        } else if (this.x < 0 || this.x > 900 || this.y < 0 || this.y > 900) {

            this.destroy(app);
        } else {

            if (this.team === 1) {

                for(let i = 0; i < EnemySpawnManager.enemies.length; i++) {
    
                    if (EnemySpawnManager.enemies[i]) {
    
                        const distance = Math.sqrt(Math.pow((this.x - EnemySpawnManager.enemies[i].x), 2) + 
                        Math.pow((this.y - EnemySpawnManager.enemies[i].y), 2));
    
                        if (distance < 17) {
    
                            const event = new CustomEvent("enemyhit", 
                            { 
                                detail: { 
                                    enemy: EnemySpawnManager.enemies[i],
                                    rocket: this,
                                    x: this.x,
                                    y: this.y
                                }
                            });
    
                            GameManager.canvas.dispatchEvent(event);
                            this.destroy(app);
                        }
                    }
                }  
            } else if (this.team === 0) {

                const distance = Math.sqrt(Math.pow((this.x - GameManager.player.x), 2) + 
                Math.pow((this.y - GameManager.player.y), 2));

                if (distance < 17) {
    
                    const event = new CustomEvent("playerhit", 
                    { 
                        detail: { 
                            rocket: this,
                            x: this.x,
                            y: this.y
                        }
                    });

                    GameManager.canvas.dispatchEvent(event);
                    this.destroy(app);
                }
            }

            this.x += Math.sin(this.launched.rotation) * 1.5;
            this.y -= Math.cos(this.launched.rotation) * 1.5;
            this.still.x = this.x;
            this.still.y = this.y;
            this.launched.x = this.x;
            this.launched.y = this.y;
        }
    }

    aimAtTarget(target) {

        if(!this.launch) {

            const yDelta = this.y - target.y;
            const xDelta = target.x - this.x;
            this.rotation = Math.atan2(xDelta, yDelta);
            this.launched.rotation = this.rotation;
        }
    }

    destroy(app) {

        if (this.launch) {

            StageManager.removeFromStage(this.launched, app);
        }

        if (!this.launch) {
            StageManager.removeFromStage(this.still, app)
        }
        this.ticker.stop();
        this.ticker.destroy();
    }

    /* fire() {

        this.still.x = this.x;
        this.still.y = this.y;
        this.launched.x = this.x;
        this.launched.y = this.y;
        this.x += Math.sin(this.rotation) * 5;
        this.y -= Math.cos(this.rotation) * 5;
    } */
}